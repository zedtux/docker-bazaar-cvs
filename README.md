# Docker image with the Bazaar VCS

I was needing the `bzr` command in order to import a project I did in the past,
I didn't find any decent simple Docker image, so here we are :)

This is a Debien Jessie (slim) image with *just* bazaar installed.

[Here](https://hub.docker.com/r/zedtux/docker-bazaar-cvs/) is a link to the Docker Hub page.

# Usage

You can use it with the following:

```bash
docker run --rm -it -v "$PWD":/devs --workdir=/devs zedtux/bzr bzr <arguments>
```

# Version

```bash
d run --rm -it -v "$PWD":/devs --workdir=/devs zedtux/bzr bzr --version
Bazaar (bzr) 2.7.0dev1
  Python interpreter: /usr/bin/python 2.7.9
  Python standard library: /usr/lib/python2.7
  Platform: Linux-4.9.49-moby-x86_64-with-debian-8.9
  bzrlib: /usr/lib/python2.7/dist-packages/bzrlib
  Bazaar configuration: /root/.bazaar
  Bazaar log file: /root/.bzr.log

Copyright 2005-2012 Canonical Ltd.
http://bazaar.canonical.com/

bzr comes with ABSOLUTELY NO WARRANTY.  bzr is free software, and
you may use, modify and redistribute it under the terms of the GNU
General Public License version 2 or later.

Bazaar is part of the GNU Project to produce a free operating system.
```
